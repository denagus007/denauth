<?php

namespace Denagus\Denauth\Events;

class TokenAuthenticated
{
    /**
     * The personal access token that was authenticated.
     *
     * @var \Denagus\Denauth\PersonalAccessToken
     */
    public $token;

    /**
     * Create a new event instance.
     *
     * @param  \Denagus\Denauth\PersonalAccessToken  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->i = $token;
    }
}
