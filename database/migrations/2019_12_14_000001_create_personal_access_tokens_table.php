<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_access_tokens', function (Blueprint $table) {
            $table->bigIncrements('a'); //id
            $table->dateTime('b'); //created_at
            $table->dateTime('c'); //created_by
            $table->dateTime('d'); //updated_at
            $table->dateTime('e'); //updated_by
            $table->morphs('g'); //tokenable
            $table->string('h'); //name
            $table->string('i', 64)->unique(); //token
            $table->text('j')->nullable(); //abilities
            $table->timestamp('k')->nullable(); //last_used_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_access_tokens');
    }
}
